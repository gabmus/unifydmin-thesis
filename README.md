# <a id="introduction"></a> 1. Introduction

The goal of this work is creating an application to facilitate the task of managing Linux systems. It aims to be a tool for any system administrator to perform repetitive tasks on multiple systems, as well as to monitor important parameters on them.

This application is developed using the GTK 3 toolkit, the Python 3 programming language, the Handy library by Purism and the Fabric Python library; all of which are free and open source software.

GTK is a toolkit for creating graphical user interfaces.[[1]](#references) GTK is the toolkit used by GNOME, a free and open source Desktop Environment for Unix-like operating systems[[2]](#references) and the leading Desktop Environment for the major industry standard Linux distributions (most notably: Red Hat Enterprise Linux (RHEL), CentOS, Fedora, Debian, Ubuntu).

The choice of using GTK was dictated by the fact that, as stated above, it offers the widest support in Linux and other Unix-like operating systems.

Python is an interpreted, high-level, general purpose programming language[[3]](#references). The choice of using python is twofold:

- Python is one of the most widespread programming languages[[4]](#references) as of writing this, and it is also considered by many one of the easiest to use. Given the extendible nature of Unifydmin, using Python makes integrating additional functionalities trivial for most developers and system administrators.
- Being an interpreted language, a Python program does not need to be compiled for one specific processor architecture. This ensures that Unifydmin will work on most modern architectures, including x86, ARM, RISC V and POWER, given those platforms have a working python interpreter and support for the compiled components that Unifydmin uses.

Unifydmin is built with support for the *Flatpak* packaging system. *Flatpak is a next-generation technology for building and distributing desktop applications on Linux*[[25]](#references). It is a distribution-agnostic packaging system, supported by all the main modern Linux distributions (including, but not limited to, the ones listed above). Flatpak applications are sandboxed and isolated from its host system; additional permissions have to be explicitly requested to the backend to access the file system, hardware devices or the host system's network. A Flatpak package has all of its dependencies built in. This avoids common issues related to dependency version mismatches and ensures that an application built for Flatpak always works as long as the host system supports it.

This report is structured as follows: Chapter 2 will introduce the ideas behind Unifydmin, and how it compares to its competitors; Chapter 3 will explain the backend logic and structure; Chapter 4 will illustrate the concepts behind the *Human Interface Design* and the process of creating the Graphical User Interface itself.

---

# <a id="the-ideas"></a> 2. The ideas

Unifydmin is presented as a *GNOME app for controlling multiple heterogeneous servers with ease*.

The main problem that led to creating Unifydmin is an old and well known one: managing many systems (be them servers or not) is a tedious and boring task.

More often that not, system administrators have a very simple set of tasks that they need to perform on the systems they manage. Some examples are updates, database or entire system rollbacks and backup processes. Another important task is making sure that everything is working as it should, and this requires the system administrator to be able to monitor certain parameters like resource usage, open ports and active services in real time.

To be fair, this is just a very small part of what system administrators do, but it's certainly a good common denominator for most of them.
 
To be able to automate or simplify these tasks, there are actually a variety of different tools in the market. The two main tools that Unifydmin tries to compare itself to are *Ansible* by Red Hat[[5]](#references) and the *Cockpit Project* sponsored again by Red Hat[[6]](#references). To understand how Unifydmin tries to differentiate, and why I felt the need to create it, one needs to have a basic understanding of these two other tools.

## <a id="ansible"></a> 2.1. Ansible

> Ansible is a universal language, unraveling the mystery of how work gets done. Turn tough tasks into repeatable playbooks. Roll out enterprise-wide protocols with the push of a button. Give your team the tools to automate, solve, and share.<br />
> -- <cite>Ansible website[[6]](#references)</cite>

Ansible is a command line utility for orchestration and automation. It aims to help system administrators to automate repetitive tasks.[[7]](#references).

One of the main advantages of Ansible is the use of ssh and python to communicate with the target systems, without the need to install any software on them. Ansible is installed in the administrator's system only.

Multiple systems can be stored and categorized in a simple text file that Ansible calls *inventory*, following a structure like the following:

```
[webservers]
www1.example.com
www2.example.com

[dbservers]
db0.example.com
db1.example.com
```

Once the *inventory* is defined, one can decide to run either modules or *playbooks*.

Modules are single operations or programs that can be executed on systems; some examples are package manager modules or the ping module.

*Playbooks* are YAML configuration files that can be compared to scripts. A *playbook* can chain together multiple modules to be ran on a selection of the systems defined in the *inventory*. Here is an example of a *playbook* that updates the Apache server and its configuration file on one or more systems using the *yum* package manager:

```yaml
- hosts: webservers
  vars:
    http_port: 80
    max_clients: 200
  remote_user: root
  tasks:
 . name: ensure apache is at the latest version
    yum:
      name: httpd
      state: latest
 . name: write the apache config file
    template:
      src: /srv/httpd.j2
      dest: /etc/httpd.conf
    notify:
   . restart apache
 . name: ensure apache is running
    service:
      name: httpd
      state: started
  handlers:
   . name: restart apache
      service:
        name: httpd
        state: restarted
```

## <a id="cockpit"></a> 2.2. Cockpit

Cockpit is a web-based administration interface for servers.

![Storage control panel in Cockpit. Screenshot from cockpit-project.org](https://cockpit-project.org/images/site/screenshot-storage.png)

According to the project website, Cockpit is targeted towards new and seasoned Linux admins alike.

Differently from Ansible, Cockpit needs to be installed server-side as a package. The package itself is made to be self-contained, so that it requires low to no manual intervention. It is included in the repositories of the most common distributions[[8]](#references) including RHEL, CentOS, Debian, Ubuntu and Clear Linux.

The user authenticates using the system credentials, eliminating the need to manage additional accounts specific to Cockpit.

Cockpit tries to expose to the user the most common monitoring and administration tools available on the server through the use of a categorized interface.

Some noteworthy monitoring and administration tools available are logs, storage, networking, containers, libvirt virtual machines, accounts, systemd services, selinux and software updates, with the addition of a full terminal embedded in the web interface itself.

Additionally Cockpit is extendable, offering the tools to create custom modules[[9]](#references).

## <a id="unifydmin"></a> 2.3. Unifydmin

Taking into account the pros and cons of both Ansible and Cockpit, Unifydmin tries to meld both together, combining the best ideas of both.

Unifydmin is a desktop application for the administration of multiple servers.

![Unifydmin monitoring interface](https://gitlab.com/gabmus/unifydmin/raw/master/screenshot.png)

Similarly to Ansible, Unifydmin uses SSH to connect to servers. The collection of information such as CPU and memory usage, open ports and systemd services is realized in such a way to minimize the load on servers.

The authentication is done by using the existing SSH keys on the computer Unifydmin is running on, and there is no option to authenticate using passwords. This avoids the problem of having to save those passwords, possibly incurring in security issues.

For ease of access, it's possible to open up a terminal on the remote server by just pressing a button, and allowing to use the user's preferred terminal emulator.

Unifydmin uses a modular structure for both monitoring and administrative operations, allowing to create custom ones easily and without having to worry about the user interface.

---

# <a id="the-backend-logic"></a> 3. The backend logic

Unifydmin implements a modular logic wherever possible, creating layers of abstractions one on top of the other to gradually expose functionality to the higher layers in a simpler and more easily maintainable way.

The whole backend logic (internally referred to as *core*) is based around three main abstractions:

- System
- Action
- Watcher (or Watcher Action)

On top of those, there are two utility structures:

- Broker (or Action Broker)
- Watcher Runner

Their main purpose is to coordinate Systems, Actions, Watchers and their respective asynchronous threads.

## <a id="system"></a> 3.1. System

System is the core abstraction of Unifydmin. It represents a user added remote machine.

The class itself is fairly simple, but manages to create an abstraction layer over Actions and Watchers.

An instance of System contains three strings:

- a display name that will be shown in the interface;
- the IP address or domain name associated with it;
- the username for the SSH connection.

The main component of a System object is the `connection` attribute. It is an instance of the `Connection` class from the *Fabric* Python library (<https://www.fabfile.org/>).

```python
self.connection = Connection(
    host = self.address,
    user = self.username
)
```

<!-- @center@ -->

*A snippet from System's constructor, where the connection object is initialized*

<!-- @endcenter@ -->

The `Connection` constructor automatically manages the authentication and connection via SSH, and it provides a higher level interface to execute remote commands.

The authentication is done by using SSH compatible keys that the user needs to set up on both the client and the server beforehand.

The System class further simplifies Fabric's interface, providing just two methods to the higher levels: `run(cmd)` and `run_as_root(cmd)` where `cmd` is a string containing a valid command to run on the System.

```python
def run(self, cmd: str) -> Result:
    out = None
    try:
        out = self.connection.run(cmd, hide=True, warn=True)
        self.__reconnected()
        return out
    except timeout:
        print(f'System {self.__repr__()}: Timeout running command `{cmd}`')
        self.__disconnected()
        return None

def run_as_root(self, cmd: str) -> Result:
    out = None
    try:
        root_connection = Connection(f'root@{self.address}', connect_timeout=15)
        root_connection.open()
        out = root_connection.run(cmd, hide=True, warn=True)
        root_connection.close()
        self.__reconnected()
        return out
    except Exception as e:
        print(f'System {self.__repr__()}: Error running root command `{cmd}`')
        print('   ', e)
        self.__disconnected()
        return None
```

<!-- @center@ -->

*A code snippet showing both run() and run_as_root()*

<!-- @endcenter@ -->

The two commands are separate for a reason: the main login operation in Unifydmin is done through a normal user. This is mainly to minimize security risks.

The normal `run(cmd)` operation uses the fabric Connection object that has already been created in the constructor. Since it will be largely used for monitoring operation, there is no need to open and close it repeatedly, causing an unnecessary overhead on both the client and the remote machine.

The `run_as_root(cmd)` operation creates a new fabric Connection object, this time authenticating as root. A privileged running option is clearly needed for some operations like updating or rebooting. Once the command is executed, the connection is closed, to minimize the time the root user actually stays connected.

The choice of implementing privileged access with root login is certainly a compromise, but at the time of taking this decision, a better choice has not been found. The main problems with root login are:

- Root login has to be enabled on the server (and in many cases, it's disabled by default)
- An SSH key has to be set up for the root user on the server

Different solutions have been evaluated, but the current one seemed like the best overall option. One way could have been to use `sudo`, and ask the user for a password each time he needs to run a privileged operation. This option was quickly discarded, as running even a single privileged command on multiple machines would require to ask a password for all of them. Another option was to ask the user to setup *passwordless sudo*, but it's more complicated than the current solution, and in general it's not a great security practice.

A System object does also contain its own list of Watcher objects that operate on it. The System itself does not directly call Watchers, it's the other way around: the Watcher object calls the System object. This will be further detailed in [Chapter 3.3. Watcher](#watcher).

The only reason why System objects keep this Watchers list is for convenience, as the user never directly interacts with a Watcher, but instead will select the System to look at the active Watchers.

It's also worth noting that the System class also contains an object that is referred to as *signaler*. Various kind of signalers are scattered throughout the project, but they all share the same idea.

A signaler is a `GObject.Object` and contains no more than a dictionary of *gsignals* that can be emitted and connected from and to the signaler itself. This is useful to facilitate the interaction with other objects of type `GObject.Object` that follow the same logic, namely all classes pertaining to GTK and GTK widgets. Using signals is vital to be able to maintain stability and responsiveness of a desktop application, especially when multiple intricate asynchronous threads are involved.

```python
class SystemSignaler(GObject.Object):
    __gsignals__ = {
        'unifydmin_system_disconnected': (
            GObject.SIGNAL_RUN_FIRST,
            None,
            (str,)
        ),
        'unifydmin_system_reconnected': (
            GObject.SIGNAL_RUN_FIRST,
            None,
            (str,)
        )
    }
```

<!-- @center@ -->

*A code snippet showing the SystemSignaler class*

<!-- @endcenter@ -->

## <a id="action"></a> 3.2. Action

Actions are executable operations (or chain of operations) that can be run on one or more systems. Through Actions the user can automate all kinds of administration tasks, depending on the need.

The structure of an Action comprehends a name and a brief description for easy recognition.

Actions have been structured in such a way to be able to optionally accept a list of arguments, although this feature is still not implemented in the GUI.

An Action object creates its own asynchronous thread and returns it with the `run(system)` method. This way the broker can manage them and see when they're done running.

```python
def run(self, system) -> threading.Thread:
    t = threading.Thread(
        name = f'{type(self)} worker thread',
        group = None,
        target = self._async_worker,
        args = (system,)
    )
    t.start()
    return t
```

<!-- @center@ -->

*A code snippet of the run() method of an Action that returns its thread*

<!-- @endcenter@ -->

The output of the last execution of an Action is stored within the action itself. This way it can be easily shown in the GUI.

The Action class itself is meant to be a parent class for actual Actions. Children Actions that the user could be able to run need to override the constructor, invoking Action's one to provide a name and a brief description. They also need to override `_async_worker()`, that will constitute the actual body of the Action, containing the commands to be run on the remote server as well as the necessary client side processing of the raw data coming down from the server.

```python
def _async_worker(self, system):
    raise NotImplementedError('This Action hasn\'t implemented _async_worker')
```

<!-- @center@ -->

*`_async_worker()` raises a NotImplementedError by default*

<!-- @endcenter@ -->

Currently the only two implemented Actions are *UpdateSystem* and *RebootSystem*. They are very simple proofs of concept and can serve as boilerplate for anyone who wants to implement any custom Action.

```python
class UpdateSystem(action.Action):
    def __init__(self):
        super().__init__(
            name = 'Update System',
            description = 'Updates the packages, has built-in middleware',
            arguments = None
        )

    def _async_worker(self, system):
        distro = get_distro(system)
        distroname = distro['os'].lower()
        if distroname in ['debian', 'ubuntu']:
            update_cmd = 'apt update && apt upgrade -y'
        elif distroname in ['arch', 'manjaro', 'antergos', 'kaos']:
            update_cmd = 'pacman -Syu --noconfirm'
        elif distroname in ['fedora', 'rhel', 'centos']:
            # check if the system has dnf, else fallback to yum
            if system.run('command -v dnf').return_code == 0:
                update_cmd = 'dnf upgrade --refresh -y'
            else:
                update_cmd = 'yum update -y'
        else:
            raise NotImplementedError(f'{distroname} currently unsupported')
            return
        self.last_result = system.run_as_root(update_cmd)
```

<!-- @center@ -->

*UpdateSystem Action, it uses a simple middleware to adapt to different package managers*

<!-- @endcenter@ -->

## <a id="watcher"></a> 3.3. Watcher

Watcher Actions are very similar to Actions, but instead of being run on demand by the user, they're automatically and continuously running in the background.

The purpose of Watchers is to be able to monitor certain parameters of the system. The most obvious ones are CPU and memory usage, but also systemd services, open ports and docker processes. As with Actions, one can write customized Watchers without much effort.

Since Watchers are meant for monitoring, it makes sense to keep a brief history of the data that's been collected. This is why a Watcher's data is a *double-ended queue* (or deque), which is particularly optimized for rotation. The data deque is initialized with zeros, and each time a new entry is added, it's rotated and filled from the bottom end.

![A representation of the insertion of new data inside a Watcher's deque](assets/deque_rotation.svg)

The size of the data deque is defined by the value passed to Watcher's constructor, therefore each implementation of a Watcher can specify its data size. In case the size is 1, a deque is still used for compatibility, but each insertion skips the rotation and just replaces the old data with the new one. A Watcher's data can be accessed through the read only `data` property.

```python
def _add_new_data(self, n_data):
    if self.data_size == 1:
        self.__data[0] = n_data
    else:
        self.__data.rotate(-1)
        self.__data[-1] = n_data
```

<!-- @center@ -->

*`_add_new_data()` method with size check and rotation*

<!-- @endcenter@ -->

A Watcher's logic has to be defined by overriding the `_worker()` method. This method is then called by the Watcher Runner, the behavior of which will be detailed in [Chapter 3.5. Watcher Runner](#watcher-runner)

## <a id="broker"></a> 3.4. Broker

Broker is a minimal class that wraps around a System-Action couple. It offers a higher level interface for both classes, specifically for running one Action on one System.

It exposes a `run()` method that starts the Action on the System and collects the thread that the Action returns. It also exposes a `done` read-only property that returns the state of the running thread.

The need for Broker comes from the fact that Unifydmin is designed to keep around only one instance of a particular Action class, and one of each System. An Action itself represents only the generic instructions to be executed, and it doesn't keep a state specific to whatever it's running.

It's Broker's job to keep a state for each execution of an Action on a certain System.

## <a id="watcher-runner"></a> 3.5. Watcher Runner

Watcher Runner is a singleton class that runs all the Watcher actions on the provided Systems.

To minimize the load on both the client and the remote System, all Watchers are run synchronously between each other, but in a thread separate from the main one. That's the reason why Watcher Runner is a singleton (*a class that restricts the instantiation of a class to one "single instance"*[[10]](#references)).

Watcher Runner keeps a list of System objects. The `_async_worker()` method scans through all the Systems and all the Watchers for each System to run them one after the other.

New Systems are added during startup or in runtime by user interaction using the `add_system(system)` method. Similarly, during runtime the user can decide to delete a System, therefore a `del_system_by_name(name)` method is provided to remove it from Watcher Runner as well.

Watchers run every two seconds to again minimize load, but in case the user has many Systems set up, running through all the Watchers can take some time. For this reason, Watcher Runner only waits for two seconds minus the time difference between the start of the execution and its end. In case this time is negative (running all Watchers takes more than two seconds), Watcher Runner doesn't wait at all.

```python
def _async_worker(self):
    while not self._thread_stop:
        start_time = time.time()
        for s in self.systems:
            for w in s.watchers:
                try:
                    w._worker()
                except:
                    print(f'Error running watcher `{w}` on system `{s}`')
        self.emit('unifydmin_watcher_new_data' , '')
        time_to_wait = 2 - (time.time() - start_time)
        if time_to_wait > 0:
            time.sleep(time_to_wait)
```

<!-- @center@ -->

*A code snippet showing `_async_worker()`*

<!-- @endcenter@ -->

Similarly to System, Watcher Runner has a signaler to be able to communicate with the GUI. Signalers have already been discussed in [Chapter 3.1. System](#system). The signaler in this case is used to tell the GUI thread that there is new data available to present.

Watcher Runner has a single thread that is initialized in the constructor. Once started, this thread executes `_async_worker()` and doesn't stop until the boolean variable `_thread_stop` is *True*.

The `start_thread()` method is called once the application initialization is complete, and it will simply start the thread indefinitely. The `stop_thread()` method sets the `_thread_stop` variable to *False*, and is called during the application shutdown to gracefully stop the worker thread.

---

# <a id="the-graphical-user-interface"></a> 4. The graphical user interface

Unifydmin's graphical user interface has been developed trying to follow the *GNOME Human Interface Guidelines*[[11]](#references).

The goal of Unifydmin's GUI is to provide a modular, glanceable overview of a System, while keeping an intuitive and familiar design language.

Another important objective was to make the interface responsive, so that it could be used on small form factor screens like smartphones and small tablets.

The source code relative to the GUI is divided in different files. Most of the time to each file corresponds a single class (in some cases small auxiliary classes have ben put inside other classes' files). With some exceptions, each class extends a GTK widget, creating new ones specific for Unifydmin.

## <a id="the-design-process"></a> 4.1. The design process

![Final mockup of Unifydmin](assets/gui-mockup.png)

Before implementing the GUI, there has been a design study on the various components of the interface.

The final design of Unifydmin is heavily inspired by another GTK application called *Fractal*[[12]](#references). It is a very familiar interface for many potential users, since most messaging applications use a similar visual paradigm.

![Screenshot of the Fractal messaging application](assets/fractal-4.0.png)

The mockup was created using the Inkscape[[13]](#references) vector graphics editor and a collection of assets from various GNOME Design repositories[[14]](#references).

While the final design of Unifydmin doesn't reflect completely the initial mockups, the time spent creating them was vital in the development process. Instead of having to come up with the design while actually writing code, by having mockups as a guideline it was possible to create the GUI just thinking about implementing a design that was already done.

## <a id="the-interface-structure"></a> 4.2. The interface structure

The interface is divided in two main interactive parts: a *sidebar* and a *main view*.

![A screenshot of the sidebar](assets/screenshots/sidebar.png)

The sidebar contains a list of available Systems and Actions, with headers for each of the two sections to be able to easily tell them apart. The two sections can also be collapsed for convenience. Each element in the sidebar has an automatically generated icon for easy recognition. Said icon consists of a circle (with the default *Adwaita* theme, this circle is dark, it can change depending on the theme set by the user) with two initials of the System or Action name.

![A screenshot showing the *Add new system* popover](assets/screenshots/add_system.png)

The *headerbar* section relative to the sidebar has two buttons, the one on the left with a `+` (plus sign) icon on it opens a *popover* menu for adding a new System by providing a display name, a username and an address. The one on the right, with a `≡` (triple bar sign) opens the general menu of the application, and it contains the following: Settings, Shortcuts, About and Quit.

The main view shows the details of the System or Action selected in the sidebar.

![A screenshot showing the data from the Watchers of a System](assets/screenshots/systemview.png)

When a System is selected the main view shows all of the data collected from the various Watchers active on the System. Data from Whatchers that have a data size greater than 1 are plotted in line charts in the top section of the view, similarly to how many popular system monitors work. The remaining data is shown in tables below the charts. Since the data in the tables tends to be pretty long, each table section can be collapsed. If a particular Watcher doesn't apply to the selected System (for instance, the one showing Docker processes only works if Docker is installed) the relative section simply won't be shown.

![A screenshot showing the System details popover](assets/screenshots/system_menu.png)

When showing a System, the headerbar section relative to the main view has two buttons and a label. The label shows the name of the System. The button with a `≡` (triple bar sign) opens a popover showing the username and address used for the System, separated by an `@` (at sign), and a button for deleting the System. The highlighted button (shown in blue with the default *Adwaita* theme, it can change depending on the theme set by the user) with the ![](assets/utilities-terminal-symbolic.svg) (terminal symbolic) icon opens a terminal emulator window inside of which an SSH connection is opened to the selected System. To ensure a wider out of the box compatibility, the default terminal emulator used is `xterm`, with some more sane defaults for a better experience like a bigger font and darker colors. The user can easily change the terminal emulator used in the settings window.

![A screenshot showing the selection of Systems when running an Action](assets/screenshots/action_prerun_w_popover.png)

When an Action is selected the main view shows a list of all Systems. From here the user can select the System(s) on which to execute the Action. When an Action is selected, the headerbar section relative to the main view has two buttons and a label, similarly to when a System is selected. The label shows the name of the selected Action. The button with a `≡` (triple bar sign) opens a popover showing a brief description of the Action, and a *Select All* button to select all Systems available. The highlighted button labeled *Run* runs the Action on the selected Systems.

![A screenshot showing an Action view while the Action is running](assets/screenshots/action_between_running.png)

When an Action is running, the main view changes. The Systems on which the action is still running show a spinner, the ones that are done show a `✓` (check mark symbol), along with a button labeled *Output*. When this button is clicked, it reveals a text view showing the output of the commands used in the Action. When clicked again, the text view is collapsed. As long as there are actions running, the *Run* button in the main view headerbar becomes inactive, and the label changes to *Running*. If the user selects any other Action while there is still one running, the *Run* button is still inactive, and the label changes to *Busy*. This is done to prevent conflicting actions to run at the same time.

## <a id="creating-the-gui"></a> 4.3. Creating the GUI

This chapter will detail the inner workings of the GUI, and the structure of the code behind it. Each sub-chapter will cover a widget, describing the most important parts of it along with any additional class or widget it makes use of.

### <a id="application"> 4.3.1. Application

Unifydmin's GUI, as the rest of the application, is initialized and started in the `__main__.py` file. In this file the `Application` class is defined, extending `Gtk.Application`. In the constructor the objects `conf_manager` and `window` are initialized, respectively as a `ConfManager` object (configuration manager, detailed in [Chapter 4.3.2.](#configuration-manager)) and as a `UnifydminAppWindow` object (the class representing the application window, detailed in [Chapter 4.3.3.](#application-window)). Also in the constructor of `Application` the `destroy` signal of the `window` object (emitted when the *close* button on the window is clicked) is connected to a method that quits the whole application.

In the `do_startup()` method, after calling the parent `Gtk.Application.do_startup()` method, the *actions* of the application are defined. Despite sharing the same name, the *actions* I'm referring to here are not the same I described in [Chapter 3.2](#action), but instead they are `Gio.SimpleAction` objects. They simply allow to connect a name like `settings`, `shortcuts` etc. to a function. Once the actions are created they are added to `Application`, to later be used to create the main application menu as described in the third paragraph of [Chapter 4.2](#the-interface-structure).

In the `do_activate()` method, after adding a little CSS for some aesthetic tweaks, the application window is presented to the user.

The `on_destroy_window()` method is responsible for shutting down the application. The first thing it does is hiding the application window, so that to the user sees it going away without waiting for the relatively long (usually a couple of seconds) shutdown sequence to complete. Then through the `conf_manager` object, the Watcher Runner's thread is stopped as described in [Chapter 3.5](#watcher-runner). Before the `quit()` method is called, one last thing needs to be done in order to make the application shutdown look as good as possible:

```python
while Gtk.events_pending():
    Gtk.main_iteration()
```

These two lines make sure that every pending GTK event is done before quitting the application. In practical terms, this mostly makes sure that the window close animation is visible before actually quitting the application.

### <a id="configuration-manager"> 4.3.2. Configuration Manager

The Configuration Manager or `ConfManager` class is a high level interface for interacting with Unifydmin's configuration file and the *core* backend of the application.

Many different components of the application need to access it and its properties, that's why it's a Singleton. This way every time a new instance of it is created, it is really returning the same instance. This avoids the problem of passing an instance around to the various components, and it also makes it act as a sort of global state that's easily shared across the whole application.

`ConfManager` also has a Signaler object (already discussed in [Chapter 3.1. System](#system)). It is used merely to emit the `unifydmin_system_deleted` signal. This signal is connected to the `del_system(signaler, name)` method of the `UnifydminAppWindow` class (detailed in [Chapter 4.3.3.](#application-window)) that removes the deleted System from the GUI.

`ConfManager` has a static variable called `BASE_SCHEMA`. This is a dictionary representing the default state of the configuration file. Using Python's dictionaries is particularly convenient for configuration files, as they are effortlessly converted to the *JSON* (JavaScript Object Notation) format using the `json` module, part of the Python Standard Library[[15]](#references).

```python
BASE_SCHEMA = {
    'systems': [
        
    ],
    'terminal_cmd': 'xterm -fa "Monospace" -fs 14 -bg "#1d1f21" -fg "#c5c8c6" -e'
}
```

<!-- @center@ -->

*The `BASE_SCHEMA` dictionary*

<!-- @endcenter@ -->

Upon startup, `ConfManager` checks if the configuration file exists or not. It is located in `.config/org.gabmus.unifydmin.json` inside the user's home directory in case the application is running from source or if it has been installed as a traditional package, or in `$XDG_CONFIG_HOME/org.gabmus.unifydmin.json` in case it's installed as a Flatpak. If the file doesn't exist, the configuration is set to `BASE_SCHEMA` and the file is created with the content of `BASE_SCHEMA`. If the file does exist, it's compared to `BASE_SCHEMA` to verify that it contains all of its keys. If one of the keys isn't found, it's added with its default value. At the end of this process the `conf` dictionary of `ConfManager` will contain the configuration data.

Inside `ConfManager` are also defined two lists, `actions` and `watcher_actions`. These lists contain respectively the Actions and Watchers that are currently enabled in the application. The Watcher Runner is also initialized here.

`ConfManager` also exposes two methods for managing Systems: `add_system(n_system)` and `del_system_by_name(name)`. The first one accepts a dictionary with `name`, `address` and `username` keys; if a System with the same name doesn't exist, then the method goes on to create a System object and save it to the configuration file. The second method accepts a string representing a System's name. After finding it, it removes it from the configuration file and emits the `unifydmin_system_deleted` signal.

### <a id="application-window"> 4.3.3. Application Window

The `UnifydminAppWindow` class extends `Gtk.ApplicationWindow` and represents the main application window of Unifydmin. It is the main widget on top of which the whole application is built. It contains a `ConfManager` object to retrieve Systems and Actions.

It also contains a `Gtk.Box` object (a simple container that can have multiple children, arranged horizontally or vertically) that contains the window content. The reason why widgets haven't been added directly to the window is for convenience in case in the future other sibling widgets were to be added.

The widget added to the box is a `UnifydminLeaflet` object referred to as *main leaflet*. Its functioning will be discussed in [Chapter 4.3.4. Leaflet](#leaflet).

There are two objects of type `Gtk.SizeGroup`: `size_group_left` and `size_group_right`. Size groups make it so that a group of widgets resizes together. These size groups are used for the *sidebar* and *main view* (as described in [Chapter 4.2.](#the-interface-structure)) respectively, so that in each section both the view and the headerbar have the same size.

The Application Window is also responsible for initializing and populating its children, as well as for responding to some of their signals, like managing the *folding* and *unfolding* of the Leaflet (detailed in [Chapter 4.3.4.](#leaflet)), responding to new selections in the sidebar or responding to the deletion of a System.

The Application Window also manages the keyboard shortcuts by defining keystrokes and their relative callbacks.

![The *Shortcuts* window shows all available keyboard shortcuts](assets/screenshots/shortcuts.png)

### <a id="leaflet"> 4.3.4. Leaflet

The `UnifydminLeaflet` class extends `Handy.Leaflet`. The leaflet widget is part of the Handy library by Purism[[16]](#references), *A library full of GTK+ widgets for mobile phones* (direct quote from Handy's repository).

![The behavior of the leaflet widget is based on the metaphor of a paper flyer (or leaflet) folding onto itself](assets/leaflet_metaphor.svg)

Leaflet is defined as *An adaptive container acting like a box or a stack*[[17]](#references). Its main purpose is to offer a widget that can adapt depending on the window (or screen) size. In normal conditions it acts just like a `Gtk.Box`, displaying its children horizontally next to each other. When the window width is low enough in such a way that it's not able to fit the two children next to each other, then the Leaflet *folds*, only showing one of the two children at a time.

![A render of how Unifydmin would look on a smartphone screen](assets/screenshots/phone_frame_left_right.png)

When the leaflet is folded, `UnifydminAppWindow` catches the `notify::folded` signal the leaflet itself emits, and shows a *back* button in the main view headerbar. This button will let the leaflet show the sidebar to be able to select another System or Action.

`UnifydminLeaflet` implements a `switch_visible()` method that toggles the visible child, a `set_visible_by_name(name)` method that accepts a string equal to either `sidebar` or `stack` to show respectively the sidebar and the main view, and a `folded` read only property that is *True* if the leaflet is folded or *False* if it is unfolded. These are simple methods added for convenience.

### <a id="leaflet"> 4.3.5. Sidebar

The `UnifydminSidebar` class extends `Gtk.Bin`, which is an essential container that only has one child. Like with many other widgets the layout of the sidebar has been drawn with *Glade*, a graphical user interface builder for GTK[[18][19]](#references). Glade allows to graphically build GTK GUIs with simple drag and drop mechanics, and allows to set some common properties for widgets. Glade creates XML files that can be made into actual working GTK interfaces by using a `Gtk.Builder` object. A similar structure will be found in many widgets using Glade files or similarly built XML interface files.

Glade also gives the possibility to bind signals emitted by the widgets to methods by just specifying the methods names. The `Gtk.Builder` object then connects those signals to an object containing the methods specified inside of glade with `builder.connect_signals(signal_handler_object)`. In Unifydmin the signal handlers are the same objects that contain the `Gtk.Builder` object.

The Glade generated XML files (as well as icons in the SVG format) are loaded by using GResources. Quoting from GResource's page in GIO Reference Manual[[20]](#references):

> Applications and libraries often contain binary or textual data that is really part of the application, rather than user data. [...] These are often shipped as files in $datadir/appname, or manually included as literal strings in the code.

> The GResource API and the glib-compile-resources program provide a convenient and efficient alternative to this which has some nice properties. You maintain the files as normal files, so its easy to edit them, but during the build **the files are combined into a binary bundle that is linked into the executable**. This means that loading the resource files are efficient (as they are already in memory, shared with other instances) and simple (no need to check for things like I/O errors or locate the files in the filesystem)

GResources are listed in the `org.gabmus.unifydmin.gresource.xml` file like so:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<gresources>
  <gresource prefix="/org/gabmus/unifydmin">
    <file compressed="true">org.gabmus.unifydmin.service.desktop</file>

    <file preprocess="xml-stripblanks">aboutdialog.glade</file>
    <file preprocess="xml-stripblanks">ui/sidebar.glade</file>
    <file preprocess="xml-stripblanks">ui/newSystemWidget.glade</file>
    <file preprocess="xml-stripblanks">ui/actionView.glade</file>
    <file preprocess="xml-stripblanks">ui/actionViewHeaderbar.glade</file>
    <file preprocess="xml-stripblanks">ui/systemView.glade</file>
    <file preprocess="xml-stripblanks">ui/systemViewHeaderbar.glade</file>
    <file preprocess="xml-stripblanks">ui/leftHeaderbar.glade</file>
    <file preprocess="xml-stripblanks">ui/settingsWindow.glade</file>
    <file preprocess="xml-stripblanks">ui/revealerWithArrow.glade</file>
    <file preprocess="xml-stripblanks">ui/shortcutsWindow.glade</file>

    <file preprocess="xml-stripblanks">icons/org.gabmus.unifydmin.svg</file>
    <file preprocess="xml-stripblanks">icons/org.gabmus.unifydmin-symbolic.svg</file>

  </gresource>
</gresources>
```

The process of combining them is automated thanks to the support in the Meson build system, used to build the entire application. Retrieving Gresources is as easy as using the right method (`Gtk.Builder.new_from_resource(path)` in case of creating a `Gtk.Builder` object) and using a path like this: `/org/gabmus/unifydmin/ui/sidebar.glade`. This path is built by using the prefix specified in the Gresource XML file like the one shown earlier (`/org/gabmus/unifydmin` in this case) followed by the path of the file that has to be retrieved, as written in the Gresource XML file (`ui/sidebar.glade` in this case).

Once the `Gtk.Builder` object is created, widgets can be retrieved by using the ID assigned to them inside of Glade like so: `builder.get_object(widget_id)`.

The parent container of the sidebar widget is a `Gtk.Box` that is added right away to `UnifydminSidebar` (which again is a `Gtk.Bin`).

After the initialization of the `UnifydminSidebar` object (done inside of [`UnifydminLeaflet`](#leaflet)), the `set_stacks(main_stack, headerbar_stack)` method needs to be called, to allow the sidebar to control what is shown inside of the main view. When the user selects a System or an Action from the sidebar, the `on_systemsListbox_row_activated(listbox, row)` or `on_actionsListbox_row_activated(listbox, row)` method are called respectively, changing the visible child of both `main_stack` and `headerbar_stack` (detailed in [Chapter 4.3.7.](#app-stack)), and emitting the `unifydmin_stack_changed` signal. This signal is connected to a method inside of [`UnifydminAppWindow`](#application-window) that changes the visible child in [`UnifydminLeaflet`](#leaflet).

Another thing that needs to be done after the sidebar initialization is populating the `Gtk.ListBox` objects relative to Systems and Actions. This is done inside of [`UnifydminAppWindow`](#application-window) by using the `populate_systems_list(systems)` and `populate_actions_list(actions)` methods of `UnifydminSidebar`. These two methods do a very similar job, therefore they call the private method `__populate_generic_list(items, listbox, rowclass)` that generalizes it. 

The sidebar also needs to control the *Systems* and *Actions* `Gtk.Revealer` objects that collapse/expand the two sections. This is done with the following methods:

- `on_systemsRevealerTrigger_button_press_event()`
- `on_actionsRevealerTrigger_button_press_event()`.

Those methods make use of the `change_arrow(arrow, state)` method to conveniently change the orientation of the arrows depending on the revealer state.

The two methods `select_next_row` and `select_prev_row` are called when using the *Shift J* and *Shift K* keyboard shortcuts respectively to select the next or previous System or Action.

Inside of the same file is also defined the `LeftHeaderbar` class, that defines the headerbar section relative to the sidebar. Similarly to `UnifydminSidebar` it also extends `Gtk.Bin`, and uses a Glade XML file for its interface.

`LeftHeaderbar` also contains the application menu, containing the *Settings*, *Shortcuts*, *About* and *Quit* actions. Their layout is defined inside of an XML string that is loaded by a `Gtk.Builder` object. The XML for the menu was put inside of a string for the simple reason that it's small enough, making it unnecessary to put it in its own file. Each of the menu actions is connected to the actions defined in the `Application` class as detailed in [Chapter 4.3.1. Application](#application).

### <a id="sidebar-listbox-row"></a> 4.3.6. Sidebar ListBox Row

Inside the sidebar, the selectable widgets that represent Systems and Actions are ListBox Rows. In particular they are `UnifydminSidebarListboxRow` objects. The class itself extends `Gtk.ListBoxRow` and its constructor requires a `name` parameter. This name is what will be actually shown in the row.

The circle on the left with the name's initials is another widget called `UnifydminInitialsIcon`. It extends `Gtk.Bin` and its constructor also requires a name. The icon itself is made up of two widgets: a `Gtk.Image` and a `Gtk.Label`. They are arranged one on top of the other using another widget specifically made for this purpose, `Gtk.Overlay`.

The actual classes used in the sidebar to build the ListBoxes extend `UnifydminSidebarListboxRow`. They are `UnifydminSidebarSystemListBoxRow` and `UnifydminSidebarActionListBoxRow`. The former takes a System object in its constructor instead of a name and adds an icon to show when a System is unreachable; the latter doesn't add anything to the class it extends, but takes an Action object in its constructor instead of a name.

### <a id="app-stack"></a> 4.3.7. App Stack

The `UnifydminAppStack` class extends `Gtk.Stack`. Quoting from GtkStack page in GTK+ 3 Reference Manual[[21]](#references):

> The GtkStack widget is a container which only shows one of its children at a time. [...] Transitions between pages can be animated as slides or fades.

`UnifydminAppStack` is the widget used in the main view of Unifydmin. It is initialized inside [`UnifydminLeaflet`](#leaflet) and added as its right child. It doesn't add much to its base class `Gtk.Stack`, except for a couple of properties. There is also a convenience method `add_and_get_stackchild(name, widget)` that creates a new `StackChild` object (defined in the same file, just a simple `Gtk.Bin` with a name property) with the widget and name passed to the method, adds it to the stack and returns it back.

The `UnifydminAppStack` class is also used for the headerbar relative to the mainview. Since each item in the main view stack is unique, its headerbar has to be as well. This right headerbar stack is initialized in [`UnifydminAppWindow`](#applocation-window). Every time the main view changes (a new System or Action is selected from the sidebar) the main app stack changes as well as the headerbar stack.

Any item in the main view stack can either be `UnifydminSystemView` or `UnifydminActionView`. These classes will be detailed in [Chapter 4.3.8.](#system-view) and [Chapter 4.3.11.](action-view) respectively

### <a id="system-view"></a> 4.3.8. System View

The `UnifydminSystemView` class extends `Gtk.Bin` and it is one of the views of the main view representing a System. Each object contains a `system` variable, that is the System object the view is referred to.

Its layout is defined using a Glade XML file (as detailed in [Chapter 4.3.5. Sidebar](#sidebar)). Its main components are a `Gtk.FlowBox` (`flowbox`) and a `Gtk.Box` (`tablebox`), both of which are inside of a `Gtk.ScrolledWindow`.

A `Gtk.ScrolledWindow` is a container that allows its child to be scrolled vertically and/or horizontally (depending on the preference set). This particular `Gtk.ScrolledWindow` has been set to only allow vertical scrolling.

Both the `flowbox` and `tablebox` objects contain visualizations of the System's Watchers. The `flowbox` contains line charts for Watchers with a data length greater than one (like CPU and Memory usage). The `tablebox` contains tables for Watchers with data length equal to one. The line charts are `UnifydminGraphBox` widgets, while the tables are `UnifydminTableView` widgets. They will be detailed in [Chapter 4.3.9.](#graph-box) and [Chapter 4.3.10.](table-view) respectively.

`UnifydminSystemView` also contains a method called `on_shift_enter()` that responds to the *Shift Enter* keystroke by opening a terminal window with an SSH connection to the System.

Inside the same file there's also another class called `SystemViewHeaderbar`. This class represents the headerbar section relative to a System View. It also uses a Glade XML file for the layout. Other than defining the layout of the headerbar, this class is also responsible for opening a terminal window with an SSH connection to the System by using the `on_systemSshBtn_clicked()` method. This method builds a command using the preferred terminal emulator set by the user (where `xterm` is the default) and the ssh command followed by the username and the address from the System object. This command is launched directly using the `Popen` method of the `subprocess` Python library if Unifydmin is running as a system application. Otherwise, if it's running as a Flatpak, it uses the `flatpak-spawn --host` command to launch a command outside of the Flatpak sandbox.

### <a id="graph-box"></a> 4.3.9. Graph Box

The `UnifydminGraphBox` class extends `Gtk.Bin` and it's responsible for drawing the line charts in the [System View](#system-view). This class relies on the `GraphView` widget, part of the Dazzle library by GNOME. Quoting from Dazzle GitLab page[[22]](#references):

> The libdazzle library is a companion library to GObject and Gtk+. It provides various features that we wish were in the underlying library but cannot for various reasons.

While Dazzle can be considered an unstable library to rely on, it's been widely used by many projects in the past, and besides thanks to Flatpak and similar packaging systems, it's possible to freeze Dazzle (as well as other libraries) to a specific version to avoid any incompatibility with newer versions.

`UnifydminGraphBox` is little more than a wrapper for Dazzle `GraphView` class, handling its initialization and presenting a higher level, easy to use widget that only needs a Watcher object to be initialized.

This class also connects to the `unifydmin_watcher_new_data` signal emitted by [`WatcherRunner`](#watcher-runner) to update the line chart with the latest data.

### <a id="table-view"></a> 4.3.10. Table View

The `UnifydminTableView` extends `Gtk.Bin` and it's the widget that represents the tables in the [System View](#system-view). The table view is built around the `Gtk.TreeView` widget, the scope of which is to create interactive tables, with or without nested objects (therefore the name *Tree View*).

The entirety of the widget is contained inside another widget, defined in the same file and called `RevealerWithArrow`. This also extends `Gtk.Bin` and its layout is defined with a Glade XML file. It consists of a `Gtk.Label` representing the title of the table view, an arrow on the far right of it, indicating that the whole table view can be collapsed and a `Gtk.Revealer`. The revealer is the widget that makes it possible to collapse the whole widget onto itself.

The `Gtk.TreeView` object is contained inside of a `Gtk.ScrolledWindow` set to only allow horizontal scrolling. Since the columns of data shown in this widget can be many, and Unifydmin is made to work even on small form factor devices, horizontal scrolling allows easy consultation of the whole set of data.

`UnifydminTableView` also connects to the `unifydmin_watcher_new_data` signal emitted by [`WatcherRunner`](#watcher-runner) to update the data in the table, but differently from [`Graph Box`](#graph-box), it checks if the data changed at all between updates before actually updating the view. This is done to save some resources and avoid unnecessary redraws.

### <a id="action-view"></a> 4.3.11. Action View

The `UnifydminActionView` class extends `Gtk.Bin` and it is one of the views of the main view representing an Action. Each object contains an `action` variable, that is the Action object the view is referred to.

The layout is defined with a Glade XML file. It's composed of a `Gtk.ScrolledWindow` widget containing a `Handy.Column` widget. Quoting directly from the Handy Reference Manual relative to HdyColumn[[23]](#references):

> The HdyColumn widget limits the size of the widget it contains to a given maximum width. The expansion of the child from its minimum to its maximum size is eased out for a smooth transition. If the child requires more than the requested maximum width, it will be allocated the minimum width it can fit in instead.

Inside the `Handy.Column` widget is a `Gtk.ListBox` widget. The list box will contain a selectable list of the available Systems to run the Action onto. Each row inside the list box is a `UnifydminSelectSystemsListBoxRow` widget. This class will be detailed in [Chapter 4.3.12.](#select-systems-listbox-row)

The `Handy.Column` widget is used to limit the width of the list box and make sure that the right-aligned widgets inside of it don't position themselves too far right in case the application window resized to be wider.

The `populate_systems_list()` method is called inside of `UnifydminAppWindow` on the application startup, and whenever Systems are added or removed.

The `on_actionRunBtn_clicked(button)` method is connected to the click signal of the *Run* button in the action view headerbar. Once triggered it sets the *Run* button insensitive, and changes its label to *Running*. It also emits the `unifydmin_action_running_changed` signal with the *True* value to indicate an action has begun running. This signal is then connected to a method inside of `UnifydminAppWindow` that makes every other action view's *Run* button insensitive and changes their label to *Busy*. The `on_actionRunBtn_clicked(button)` method then runs the Action on all of the selected Systems, calling on each of the `UnifydminSelectSystemsListBoxRow` widgets the `run_action()` method. When an action is completed on one System, the `done_running()` method is called on the row representing it. This makes the *Output* button available. Once the Action is done running on all Systems, the action view's own *Run* button is restored to its original state, and the `unifydmin_action_running_changed` signal is emitted again, this time with the *False* value, to restore every other action view's *Run* button as well.

In the same file is also defined the `ActionViewHeaderbar` class, that extends `Gtk.Bin` and represents the headerbar relative to the action view. Its layout is defined with a Glade XML file. The *Select All* and *Run* buttons' signals are connected to handlers inside of `UnifydminActionView`.

### <a id="select-systems-listbox-row"></a> 4.3.12. Select Systems ListBox Row

The `UnifydminSelectSystemsListBoxRow` class extends `Gtk.ListBoxRow`. They are used inside the `UnifydminActionView` class as rows for the `Gtk.ListBox` widget made to select Systems onto which to run an Action.

The constructor requires a System and an Action object that the row refers to. With these two objects, it creates a [`Broker`](#broker) object, used to run and monitor the execution of the Action on the System.

The `run_action()` method calls the `Broker.run()` method, starting its thread, and the `start_running()` method, that changes the aspect of the row, making it and its checkbox insensitive, and showing an animated spinner, indicating that the Action is running on the System.

The `done_running()` method changes the spinner into a `✓` (check mark symbol) icon, indicating that the Action is done running, shows the *Output* button and makes the row and its checkbox sensitive again, ready for a new run.

The `reset_running()` method resets the row to its original state, hiding the check mark icon and the *Output* button and making the row and its checkbox sensitive.

Inside the same file are defined a number of accessory widgets that will be detailed below.

`OutputButton` extends `Gtk.ToggleButton`. A toggle button is a switch with a button's appearance, and every time it's clicked its *active* state toggles between *True* and *False*. `OutputButton` in particular just defines a custom one with the *Output* label in it, and an arrow icon pointing up or down depending on the *active* state of the button.

`OutputRevealer` extends `Gtk.Revealer`. It contains a `GtkSource.View` widget (a widget that shows text, optimized for showing code) to show the output of the Action inside of a `Gtk.ScrolledWindow` that only allows horizontal movement, to avoid breaking the lines of the Action output. It also has a `set_data(data)` method that accepts a string and sets it as the text of the `Gtk.SourceView`. `OutputRevealer` is expanded and collapsed based on the *active* state of `OutputButton`.

`SpinnerOrCheck` extends `Gtk.Box`. It contains a `Gtk.Spinner` widget (a simple animated spinner) and a `✓` (check mark symbol) icon. It has three methods: `show_spinner()`, `show_check()` and `show_none()`. The first two show the spinner or check mark respectively hiding the other one, while the third one hides both.

### <a id=""></a> 4.3.13. Titlebar

`UnifydminLeafletTitlebar` extends `Handy.TitleBar` and it represents the entirety of the title bar or headerbar of Unifydmin. Quoting from TitleBar's page in Handy Reference Manual[[24]](#references):

> HdyTitleBar is meant to be used as the top-level widget of your window's title bar. It will be drawn with the same style as a GtkHeaderBar but it won't force a widget layout on you: you can put whatever widget you want in it, including a GtkHeaderBar. HdyTitleBar becomes really useful when you want to animate header bars, like an adaptive application using HdyLeaflet would do.

Since the headerbar is split in two parts, one of them being even a `Gtk.Stack` containing multiple headerbars, making use `Handy.TitleBar` is the best way to make them all look and behave as a single headerbar.

In `UnifydminLeafletTitlebar` there is an object of type `Handy.HeaderGroup`. The documentation on this widget isn't very comprehensive at this time, but it's fairly easy to understand. Headerbars need to be able to show window controls, depending on how the window is configured, and depending on if the main [leaflet](#leaflet) is folded or not. `Handy.HeaderGroup` simplifies this mechanic, managing where window controls are showed. It has a shortcomings, tho: a `Handy.HeaderGroup` needs to have two headerbars in it, and no more than that. And it only takes headerbars, not `Gtk.Stack`s. So every time the main stack (and consequently the headerbar stack) change, the old right headerbar needs to be removed from the header group, and the new visible one has to be added.

`UnifydminLeafletTitlebar` takes two arguments, the left headerbar and the right headerbar stack. During the application startup, the left headerbar and the first child of the right headerbar stack are added to the headergroup. The right headerbar is changed in the `on_main_leaflet_folded()` method inside `UnifydminAppWindow`.

The main child of `UnifydminLeafletTitlebar` is a `Handy.Leaflet` widget containing the left headerbar and the right headerbar stack. This leaflet is folded together with the main one and looks uniform with it.

There are two methods: `leaflet_switch_visible()` that (if the leaflet is folded) toggle between showing the left headerbar and the right headerbar stack; and `leaflet_set_visible_by_name(name)` that takes a string attribute that should be either `sidebar` or `stack` and changes (again, if the leaflet is folded) the visible child of the leaflet accordingly.

---

# <a id="references"></a> 5. References

1. The GTK Project website; <https://www.gtk.org/>
2. GNOME Wikipedia page; <https://en.wikipedia.org/wiki/GNOME>
3. Python (programming language) Wikipedia page; <https://en.wikipedia.org/wiki/Python_(programming_language)>
4. TIOBE Index; <https://www.tiobe.com/tiobe-index/>
5. Ansible; <https://www.ansible.com/>
6. Cockpit Project; <https://cockpit-project.org/>
7. Why Ansible?; <https://www.ansible.com/overview/it-automation>
8. Running Cockpit; <https://cockpit-project.org/running.html>
9. Starter Kit. the turn-key template for your own pages; <https://cockpit-project.org/blog/cockpit-starter-kit.html>
10. Singleton pattern Wikipedia page; <https://en.wikipedia.org/wiki/Singleton_pattern>
11. GNOME Human Interface Guidelines; <https://developer.gnome.org/hig/stable/>
12. Fractal, Matrix messaging client; <https://wiki.gnome.org/Apps/Fractal>
13. Inkscape; <https://inkscape.org/>
14. GNOME Design repository group; <https://gitlab.gnome.org/Teams/Design>
15. The Python Standard Library; <https://docs.python.org/3/library/index.html>
16. Handy; <https://source.puri.sm/Librem5/libhandy>
17. HdyLeaflet documentation; <https://honk.sigxcpu.org/projects/libhandy/doc/HdyLeaflet.html>
18. Glade website; <https://glade.gnome.org/index.html>
19. Glade Interface Designer Wikipedia page; <https://en.wikipedia.org/wiki/Glade_Interface_Designer>
20. GResource's page in GIO Reference Manual; <https://developer.gnome.org/gio/stable/GResource.html>
21. GtkStack, GTK+ 3 Reference Manual; <https://developer.gnome.org/gtk3/stable/GtkStack.html>
22. Dazzle GitLab page; <https://gitlab.gnome.org/GNOME/libdazzle>
23. HdyColumn's page in Handy Reference Manual; <https://honk.sigxcpu.org/projects/libhandy/doc/HdyColumn.html>
24. HdyTitlebar's page in Handy Reference Manual; <https://honk.sigxcpu.org/projects/libhandy/doc/HdyTitleBar.html>
25. Flatpak website; <https://flatpak.org/>
